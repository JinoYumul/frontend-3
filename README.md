Front end application for CSP2's E-Commerce API

To get started, create a file in the root of the project named .env.local and add the following code:

REACT_APP_API_URL=http://localhost:4000 (or whichever port you'd like to use)